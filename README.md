IS README

Written in C#, these scripts take a user's controller 3D position tp map out a user drawn waveform to be played back in a virtual world.
This was created using Unity 3D. To learn more about what each script does in depth, read at the corresponding paper.

-DrawLineManager primarily handles capturing controller movement
-MeshLine Renderer takes the 3D drawing and limits it to a 2D wall, as well as the linear interpolation and normalization of the function to push to the sound script
-SoundScript takes the interpolated points and pushes the values to the audio buffer, allowing the change in timbre to be observed by the user