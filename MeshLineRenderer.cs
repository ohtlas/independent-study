using UnityEngine;
using System.Collections.Generic;
using System.Text;

[RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshFilter))] public class MeshLineRenderer
    : MonoBehaviour {

public
  Material lineMaterial;
  // mesh
private
  Mesh ml;
private
  Vector3 start;
private
  float lineSize = .1f;
private
  bool firstQuad = true;
public
  List<float> new_waveform = new List<float>();

  void Start() {
    // gets and keeps track of the mesh
    ml = GetComponent<MeshFilter>().mesh;
    GetComponent<MeshRenderer>().material = lineMaterial;
  }
  // sets width of the line
public
  void setWidth(float width) { lineSize = width; }

  // draws a point
public
  void AddPoint(Vector3 point) {
    if (start != Vector3.zero) {
      // Limits the drawing to the wall
      start.z = -4.8F;
      point.z = -4.8F;
      AddLine(ml, MakeQuad(start, point, lineSize, firstQuad));

      firstQuad = false;
    }
    // Sets the start for the next point
    start = point;
  }
  // Calculates the waveform to be set in the audio buffer
public
  void calcWaveform(List<Vector3> vector_list) {
    Vector3[] points_vector = new Vector3[vector_list.Count];
    for (int i = 0; i < vector_list.Count; i++) {
      points_vector[i] = vector_list[i];
    }

    Vector3[] inter_points = inter_vectors(points_vector);

    float[] point_array = VerticesToFloat(inter_points);

    float[] new_waveform_array = normalizePoints(point_array);
    for (int i = 0; i < new_waveform_array.Length; i++) {
      new_waveform.Add(new_waveform_array[i]);
    }
  }

  // Makes the quad that the line is drawn from
  Vector3[] MakeQuad(Vector3 start, Vector3 end, float width, bool all) {
    // divide width by 2
    width = width / 2;

    Vector3[] quads;
    if (all) {
      quads = new Vector3[4];
    } else {
      quads = new Vector3[2];
    }

    // calculate normal vector
    Vector3 normal = Vector3.Cross(start, end);
    // calculate line vector
    Vector3 length = Vector3.Cross(normal, end - start);
    // nomalize the cross product
    length.Normalize();

    // creates the quad
    if (all) {
      quads[0] = transform.InverseTransformPoint(start + length * width);
      quads[1] = transform.InverseTransformPoint(start + length * -width);
      quads[2] = transform.InverseTransformPoint(end + length * width);
      quads[3] = transform.InverseTransformPoint(end + length * -width);
    } else {
      quads[0] = transform.InverseTransformPoint(start + length * width);
      quads[1] = transform.InverseTransformPoint(start + length * -width);
    }
    // returns the quad
    return quads;
  }

  // Converts a vector array to a float array
  float[] VerticesToFloat(Vector3[] vector) {
    int size = vector.Length;
    float[] y_vals = new float[size];

    for (int i = 0; i < vector.Length; i++) {
      y_vals[i] = vector[i].y;
    }
    return y_vals;
  }

  // Normalizes the Y values of the list
public
  float[] normalizePoints(float[] points) {
    float max_point = points[0];
    float min_point = points[0];
    float max_scale = 1;
    float min_scale = -1;

    // Finds and sets the max value point
    for (int i = 1; i < points.Length; i++) {
      if (points[i] > max_point) {
        max_point = points[i];
      }
    }

    // Finds and sets the min value point
    for (int j = 0; j < points.Length; j++) {
      if (min_point > points[j]) {
        min_point = points[j];
      }
    }

    // Finds the range of values
    float point_range = max_point - min_point;

    // Sets the range of the scale
    float scale_range = max_scale - min_scale;

    // Creates a new float array to be returned of the same size
    // as the array passed in
    float[] normalized = new float[points.Length];

    // Normalizes the points and adds them to the new array
    for (int k = 0; k < points.Length; k++) {
      normalized[k] =
          (((scale_range * (points[k] - min_point)) / point_range) + min_scale);
    }
    return normalized;
  }

  // linear interpolation - start is y0, finish is y1, percentage is ((x - x0)/(x1 - x0))
  float lerp(float start, float finish, float percentage) {
    return (1 - percentage) * start + (percentage * finish);
  }

  // Linear interpoation of the vectors
  Vector3[] inter_vectors(Vector3[] points) {
    int num_inter_points = 109;
    // Creates a new vector array the same size as the parray that is passed in
    Vector3[] inter_points = new Vector3[num_inter_points];

    float percentage = 0.0f;

    float start_x = points[0].x;

    // Sets the x values to start at 0
    for (int j = 0; j < points.Length; j++) {
      points[j].x = (points[j].x - start_x);
    }

    // Sets the scale for the points to be interpolated
    float scale = points[points.Length - 1].x * 1000 / num_inter_points - 1;

    float x = 0.0f;
    // Handles the iteration through the points to find the correct points to be sampled along the wave
    for (int i = 0; i < num_inter_points - 1; i++) {

      // Iterates through the actual x values rather than the scale while real val x < scale val x and real val x + 1 < scale val x
      for (int k = 0; k < points.Length - 1; k++) {
        // Sets inter_points to the interpolated points if the scaled x and real x are equal
        if (points[k].x == x) {
          inter_points[i].y = points[k].y;
          inter_points[i].x = x;
          inter_points[i].z = 0;
        }

        // Sets inter_points to the interpolated points if at the correct interval
        else if (points[k].x < x && points[k + 1].x > x) {

          // percentage is ((x - x0)/(x1 - x0))
          percentage = (x - points[k].x) / (points[k + 1].x - points[k].x);

          inter_points[i].y = lerp(points[k].y, points[k + 1].y, percentage);
          inter_points[i].x = x;
          inter_points[i].z = 0;
        }
      }

      x += scale / 1000;
    }
    inter_points[num_inter_points - 1] = points[points.Length - 1];
    inter_points[num_inter_points - 1].z = 0;
    return inter_points;
  }

  // creates a line using the mesh and quads
  void AddLine(Mesh mesh, Vector3[] quad) {

    int vl = mesh.vertices.Length;

    Vector3[] vs = mesh.vertices;
    vs = resizeVertices(vs, 2 * quad.Length);

    for (int i = 0; i < 2 * quad.Length; i += 2) {
      vs[vl + i] = quad[i / 2];
      vs[vl + i + 1] = quad[i / 2];
    }

    Vector2[] uvs = mesh.uv;
    uvs = resizeUVs(uvs, 2 * quad.Length);

    // UV mapping
    if (quad.Length == 4) {
      uvs[vl] = Vector2.zero;
      uvs[vl + 1] = Vector2.zero;
      uvs[vl + 2] = Vector2.right;
      uvs[vl + 3] = Vector2.right;
      uvs[vl + 4] = Vector2.up;
      uvs[vl + 5] = Vector2.up;
      uvs[vl + 6] = Vector2.one;
      uvs[vl + 7] = Vector2.one;
    } else {
      if (vl % 8 == 0) {
        uvs[vl] = Vector2.zero;
        uvs[vl + 1] = Vector2.zero;
        uvs[vl + 2] = Vector2.right;
        uvs[vl + 3] = Vector2.right;

      } else {
        uvs[vl] = Vector2.up;
        uvs[vl + 1] = Vector2.up;
        uvs[vl + 2] = Vector2.one;
        uvs[vl + 3] = Vector2.one;
      }
    }

    int tl = mesh.triangles.Length;

    int[] ts = mesh.triangles;
    ts = resizeTriangles(ts, 12);

    if (quad.Length == 2) {
      vl -= 4;
    }

    // front-facing quad
    ts[tl] = vl;
    ts[tl + 1] = vl + 2;
    ts[tl + 2] = vl + 4;

    ts[tl + 3] = vl + 2;
    ts[tl + 4] = vl + 6;
    ts[tl + 5] = vl + 4;

    // back-facing quad
    ts[tl + 6] = vl + 5;
    ts[tl + 7] = vl + 3;
    ts[tl + 8] = vl + 1;

    ts[tl + 9] = vl + 5;
    ts[tl + 10] = vl + 7;
    ts[tl + 11] = vl + 3;

    mesh.vertices = vs;
    mesh.uv = uvs;
    mesh.triangles = ts;
    mesh.RecalculateBounds();
    mesh.RecalculateNormals();
  }

  Vector3[] resizeVertices(Vector3[] ovs, int ns) {
    Vector3[] nvs = new Vector3[ovs.Length + ns];
    for (int i = 0; i < ovs.Length; i++) {
      nvs[i] = ovs[i];
    }

    return nvs;
  }

  Vector2[] resizeUVs(Vector2[] uvs, int ns) {
    Vector2[] nvs = new Vector2[uvs.Length + ns];
    for (int i = 0; i < uvs.Length; i++) {
      nvs[i] = uvs[i];
    }

    return nvs;
  }

  int[] resizeTriangles(int[] ovs, int ns) {
    int[] nvs = new int[ovs.Length + ns];
    for (int i = 0; i < ovs.Length; i++) {
      nvs[i] = ovs[i];
    }

    return nvs;
  }
}
