using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(AudioSource))] public class SoundScript
    : MonoBehaviour {
public
  bool clear = true;

private
  int sampling_frequency;
private
  bool running = false;
private
  List<float> waveform = new List<float>();
private
  List<float> new_waveform = null;
private
  int index = 0;
  StreamWriter sw;

  void Start() {
    sampling_frequency = AudioSettings.outputSampleRate;
    Debug.Log("Sample Rate: " + sampling_frequency);

    waveform.Add(0);
    sw = new StreamWriter(
        "waveform" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-f") + ".txt");
    running = true;
  }

  // Updates the waveform from the last one to the next one given
public
  void UpdateWaveform(List<float> new_normPoints) {
    List<float> temp_waveform = new List<float>();
    for (int i = 0; i < new_normPoints.Count; i++) {
      temp_waveform.Add(new_normPoints[i]);
      sw.WriteLine(temp_waveform[i]);
    }
    sw.WriteLine("\n");
    new_waveform = temp_waveform;
    sw.Flush();
  }

  void OnAudioFilterRead(float[] data, int channels) {

    if (!running) {
      return;
    }

    // Checks to make sure that the new_waveform is not empty before loading it
    // into waveform
    if (new_waveform != null) {
      // for (int i = 0; i < new_waveform.Count; i++) {
      //   waveform.Add(new_waveform[i]);
      // }
      waveform = new_waveform;
      new_waveform = null;
      // new_waveform.Clear();
      index = 0;
    }
    // If clear is true, stop the sound from playing
    if (clear == true) {
      waveform.Clear();
      waveform.Add(0);
      new_waveform = null;
      index = 0;
      clear = false;
    }
    // Adds the values from waveform to the audio buffer to be
    // played back to the user
    for (var i = 0; i < data.Length; i = i + channels) {
      data[i] = waveform[index];
      // if we have stereo, we copy the mono data to each channel
      if (channels == 2)
        data[i + 1] = data[i];
      index = (index + 1) % waveform.Count;
    }
  }
}
