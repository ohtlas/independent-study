public
class DrawLineManager : MonoBehaviour {

public
  SteamVR_TrackedObject trackedObj;

private
  MeshLineRenderer currLine;

public
  Material lMat;

private
  Vector3 position;

private
  int numClicks = 0;

public
  bool drawing_complete = true;

public
  List<Vector3> vector_list = new List<Vector3>();

  // Update is called once per frame
  void Update() {
    SteamVR_Controller.Device device =
        SteamVR_Controller.Input((int)trackedObj.index);

    // Checks if the controller trigger is pressed down
    if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger)) {
      GameObject go = new GameObject();

      // Creates a mesh filter component and links it adds a MeshLineRenderer
      // component
      go.AddComponent<MeshFilter>();
      go.AddComponent<MeshRenderer>();
      currLine = go.AddComponent<MeshLineRenderer>();

      // Sets the material to the specified material from Unity
      currLine.lineMaterial = lMat;
      // Sets the width of the line being drawn
      currLine.setWidth(.01f);

      numClicks = 0;
    }
    // Checks if the controller trigger is held
    else if (device.GetTouch(SteamVR_Controller.ButtonMask.Trigger)) {

      // Sets position to the position of the controller
      position = trackedObj.transform.position;

      // Sets the z component of position to -4.8 which is the location of the
      // wall being drawn on
      position.z = -4.8F;

      // Adds a point at position
      currLine.AddPoint(position);

      // Fixes the x component of position to be fed into the linear
      // interpolation function
      Vector3 new_x_position = position;
      new_x_position.x = new_x_position.x * -10;

      // Adds the point vector to the list to be interpolated
      vector_list.Add(new_x_position);
      numClicks++;
    }
    // Checks if the controller trigger is released
    else if (device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger)) {

      // checks if vector_list is empty
      if (vector_list != null) {
        // Calculate the waveform to be fed into the audio buffer from the list
        // of vectors
        gameObject.GetComponent<MeshLineRenderer>().calcWaveform(vector_list);
      }
      // Sends the calculated waveform from MeshLineRenderer to SoundScript
      gameObject.GetComponent<SoundScript>().UpdateWaveform(
          gameObject.GetComponent<MeshLineRenderer>().new_waveform);
    }
  }
}
